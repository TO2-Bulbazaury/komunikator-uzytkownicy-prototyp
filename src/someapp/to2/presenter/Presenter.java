package someapp.to2.presenter;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import someapp.to2.Main;
import someapp.to2.model.Database;
import someapp.to2.model.User;
import someapp.to2.persistence.HibernateUtils;
import someapp.to2.view.ActiveUsersWindowController;
import someapp.to2.view.PreferencesWindowController;
import someapp.to2.view.MainWindowController;
import someapp.to2.view.PopupController;

import java.io.IOException;

public class Presenter {

    private Stage primaryStage;
    Database database;

    public Presenter(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.database = new Database();
    }

    public Database getDatabase() {
        return this.database;
    }

    public void initRootLayout() {
        try {
            this.primaryStage.setTitle("TO2 prototype - main window");

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/MainWindow.fxml"));
            BorderPane rootLayout = (BorderPane) loader.load();

            MainWindowController controller = loader.getController();
            controller.setPresenter(this);

            primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    HibernateUtils.shutdown();
                }
            });

            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showActiveUsersList(User user) {
        try {
            primaryStage.hide();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/ActiveUsersWindow.fxml"));
            BorderPane page = (BorderPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Active Users");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            dialogStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    HibernateUtils.shutdown();
                }
            });

            ActiveUsersWindowController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainStage(primaryStage);
            controller.setPresenter(this);
            controller.setUser(user);
            controller.setUsername();

            dialogStage.showAndWait();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showPopup() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/Popup.fxml"));
            BorderPane page = (BorderPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Some popup");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            PopupController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainStage(primaryStage);
            controller.setPresenter(this);

            dialogStage.showAndWait();

            dialogStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    System.out.println("Closing");
                    primaryStage.showAndWait();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showCheckBoxesWindow(User user) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(Main.class.getResource("view/PreferencesWindow.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Check Boxes");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            PreferencesWindowController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainStage(primaryStage);
            controller.setPresenter(this);
            controller.setUser(user);

            dialogStage.showAndWait();

            dialogStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    System.out.println("Closing");
                    primaryStage.showAndWait();
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
