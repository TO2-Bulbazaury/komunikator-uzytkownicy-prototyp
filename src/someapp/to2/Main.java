package someapp.to2;

import someapp.to2.persistence.HibernateUtils;
import someapp.to2.presenter.Presenter;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    private Stage primaryStage;
    private Presenter presenter;

    @Override
    public void start(Stage primaryStage) throws Exception{
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("TO2 prototype - main window");

        this.presenter = new Presenter(primaryStage);
        this.presenter.initRootLayout();
    }

    public static void main(String[] args) {
        HibernateUtils.getSession().close();
        launch(args);
    }
}
