package someapp.to2.model;

import java.util.HashSet;
import java.util.Set;

public class User {

    private long id;
    private String username;
    private String password;
    private Set<Preference> preferences;

    public User() {
        this.preferences = new HashSet<>();
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Preference> getPreferences() {
        return this.preferences;
    }

    public void setPreferences(Set<Preference> preferences) {
        this.preferences = preferences;
    }

    public void addPreference(Preference preference) {
        this.preferences.add(preference);
    }

    public String toString() {
        return this.username + "\t" + Caesar.encrypt(this.password);
    }

}
