package someapp.to2.model;

import org.hibernate.Session;
import org.hibernate.Transaction;
import someapp.to2.persistence.HibernateUtils;

import java.util.HashSet;
import java.util.Set;
import java.io.*;

public class Database {

    private Set<User> users = new HashSet<>();
    PrintWriter file;

    public Database() {

    }

    public void addUser(User user) {
        this.users.add(user);
    }

    public void saveToFile(String filename, String data) {
        try {
            this.file = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)));
            this.file.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            this.file.close();
        }
    }

    public void saveToDatabase(User user) {

        Session session = HibernateUtils.getSession();
        Transaction transaction = session.beginTransaction();

        session.persist(user);

        for (Preference preference : user.getPreferences()) {
            session.persist(preference);
        }

        transaction.commit();
        session.close();

    }

}
