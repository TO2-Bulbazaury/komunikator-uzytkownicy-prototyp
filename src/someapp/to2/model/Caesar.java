package someapp.to2.model;

public class Caesar {

    public Caesar() {

    }

    public static String encrypt(String string) {
        int n = string.length();
        char[] chars = new char[n];
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < n; i++) {
            chars[i] = string.charAt(i);
            chars[i] -= 2;
            sb.append(chars[i]);
        }

        String newString = sb.toString();

        return newString;
    }

}
