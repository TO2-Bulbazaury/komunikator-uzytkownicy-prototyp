package someapp.to2.model;

public class Preference {

    private long id;
    private String name;
    private boolean choice;
    private User user;

    public Preference() {

    }

    public Preference(String name, boolean chose) {
        this.name = name;
        this.choice = chose;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getChoice() {
        return this.choice;
    }

    public void setChoice(boolean choice) {
        this.choice = choice;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
