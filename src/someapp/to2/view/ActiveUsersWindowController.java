package someapp.to2.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import someapp.to2.model.User;
import someapp.to2.presenter.Presenter;

public class ActiveUsersWindowController {
    private Stage dialogStage;
    private Stage mainStage;
    private Presenter presenter;
    private User user;

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

    public void  setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    public void  setUser(User user) { this.user = user; }

    @FXML
    private Button optionsButton;
    @FXML
    private Button archivesButton;
    @FXML
    private Button logoutButton;
    @FXML
    private Button startConversationButton;
    @FXML
    private Button seeProfileButton;
    @FXML
    private Label username;


    public void setUsername(){
        username.setText(user.getUsername());
    }

    @FXML
    public void handleOptionsAction(ActionEvent event) {
        presenter.showCheckBoxesWindow(user);
    }
    @FXML
    public void handleArchivesAction(ActionEvent event) {

    }
    @FXML
    public void handleLogoutAction(ActionEvent event) {

    }
    @FXML
    public void handleStartConversationAction(ActionEvent event) {

    }
    @FXML
    public void handleSeeProfileAction(ActionEvent event) {

    }

}
