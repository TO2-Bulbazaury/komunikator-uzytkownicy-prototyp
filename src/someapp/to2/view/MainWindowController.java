package someapp.to2.view;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import someapp.to2.model.Caesar;
import someapp.to2.model.User;
import someapp.to2.persistence.HibernateUtils;
import someapp.to2.presenter.Presenter;


public class MainWindowController {

    private Presenter presenter;
    private Stage stage;

    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    private TextField usernameTextField;
    @FXML
    private TextField passwordTextField;
    @FXML
    private Button loginButton;
    @FXML
    private Button registerButton;

    @FXML
    public void handleLoginAction(ActionEvent event) {
        User user = new User();
        user.setUsername(usernameTextField.getText());
        user.setPassword(Caesar.encrypt(passwordTextField.getText()));
        System.out.println(user);

        presenter.getDatabase().saveToFile("myDatabase.txt", user + "\n");

        presenter.showActiveUsersList(user);

    }

    @FXML
    public void handleRegisterAction(ActionEvent event) {
        presenter.showPopup();
    }


    @FXML
    public void stop() {
        HibernateUtils.shutdown();
    }

}
