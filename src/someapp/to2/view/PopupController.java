package someapp.to2.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import someapp.to2.presenter.Presenter;

public class PopupController {

    private Stage dialogStage;
    private Stage mainStage;
    private Presenter presenter;

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

    public void  setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    @FXML
    private Button windowExitButton;
    @FXML
    private Button appExitButton;

    @FXML
    public void handleWindowExitAction(ActionEvent event) {
        dialogStage.close();
    }

    @FXML
    public void handleAppExitAction(ActionEvent event) {
        dialogStage.close();
        mainStage.close();
    }

}
