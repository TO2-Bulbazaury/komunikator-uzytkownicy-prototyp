package someapp.to2.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.stage.Stage;
import someapp.to2.model.Preference;
import someapp.to2.model.User;
import someapp.to2.presenter.Presenter;

public class PreferencesWindowController {

    private Stage dialogStage;
    private Stage mainStage;
    private Presenter presenter;
    private User user;

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @FXML
    CheckBox checkBox1;
    @FXML
    CheckBox checkBox2;
    @FXML
    CheckBox checkBox3;
    @FXML
    Label total;
    @FXML
    Label list;
    @FXML
    Button submit;
    @FXML
    RadioButton radioButton1;
    @FXML
    RadioButton radioButton2;
    @FXML
    RadioButton radioButton3;
    @FXML
    Label choice;

    @FXML
    public void initialize() {
        checkBox1.setSelected(true);

        int count = 0;
        String choices = "";
        if (checkBox1.isSelected()) {
            count++;
            choices += checkBox1.getText() + "\n";
        }
        if (checkBox2.isSelected()) {
            count++;
            choices += checkBox2.getText() + "\n";
        }
        if (checkBox3.isSelected()) {
            count++;
            choices += checkBox3.getText() + "\n";
        }
        total.setText("Choices amount: " + count);
        list.setText(choices);
    }

    @FXML
    public void handleCheckBoxAction(ActionEvent event) {
        int count = 0;
        String choices = "";
        if (checkBox1.isSelected()) {
            count++;
            choices += checkBox1.getText() + "\n";
        }
        if (checkBox2.isSelected()) {
            count++;
            choices += checkBox2.getText() + "\n";
        }
        if (checkBox3.isSelected()) {
            count++;
            choices += checkBox3.getText() + "\n";
        }
        total.setText("Choices amount: " + count);
        list.setText(choices);
    }

    @FXML
    public void handleRadioButtonAction(ActionEvent event) {
        String string = "";
        if (radioButton1.isSelected()) {
            string += radioButton1.getText();
        }
        if (radioButton2.isSelected()) {
            string += radioButton2.getText();
        }
        if (radioButton3.isSelected()) {
            string += radioButton3.getText();
        }
        choice.setText("Radio Button Choice:\n" + string);
    }

    @FXML
    public void handleSubmitAction(ActionEvent event) {
        System.out.println(total.getText());
        System.out.println(list.getText());
        System.out.println(choice.getText() + "\n\n\n");

        user.addPreference(new Preference("CheckBox1", checkBox1.isSelected()));
        user.addPreference(new Preference("CheckBox2", checkBox2.isSelected()));
        user.addPreference(new Preference("CheckBox3", checkBox3.isSelected()));
        user.addPreference(new Preference("RadioButton1", radioButton1.isSelected()));
        user.addPreference(new Preference("RadioButton2", radioButton2.isSelected()));
        user.addPreference(new Preference("RadioButton3", radioButton3.isSelected()));

        presenter.getDatabase().saveToDatabase(user);

    }

}
